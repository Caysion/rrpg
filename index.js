"use strict";

let game;

let gameOptions = {
    slices: 4,
    slicePrizes:["fightGame","upgradeGame","shopGame","gambleGame"],
    rotationTime:3000
};

class character{
    constructor(health,damage){
        this.health = health;
        this.damage = damage;
    }

    receiveDamage(damage){
        this.health -= damage;
    }

}

class playerClass extends character{
    constructor(health,damage){
        super(health,damage);
        this.gold = 50;
        this.maxHealth = health;
        this.maxDamage = damage;
        this.potions = 2;
        this.defense = 2;

        this.baseHealth = health;
        this.baseAttack = damage;
        this.baseDefense = this.defense;
    }
}

let player = new playerClass(5,1);
let eneHealth;
let upgraded = false;

window.onload = function(){
    let gameConfig = {
        type: Phaser.AUTO,
        width: 1000,
        height: 1000,
        parent: "Phaser-game",
        autoCenter: Phaser.Scale.CENTER_HORIZONTALLY,
        backgroundColor: 0x880044,
        scene: [playGame,fightGame,shopGame,gambleGame,upgradeGame,highGamble,lowGamble]

    };

    game = new Phaser.Game(gameConfig);

    window.focus();
    resize();
    window.addEventListener("resize",resize,false);
};

class playGame extends Phaser.Scene{
    constructor(){
        super("PlayGame");
        Phaser.Scene.call(this, {key:'playGame'});
    }

    preload(){

        this.load.image("wheel","assets/wheel1000.png");
        this.load.image("pin","assets/pin.png");
        this.load.audio("wheelComplete","assets/SoundEffects/wheelComplete.mp3");
    }

    create(){
        this.wheel = this.add.sprite(game.config.width / 2, game.config.height/2,"wheel");
        this.pin = this.add.sprite(game.config.width / 2, game.config.height/2,"pin");
        this.prizeText = this.add.text(game.config.width / 2,game.config.height - 20,"Spin the Wheel",{
            font: "bold 32px Arial",
            align: "center",
            color:"red"
        });

        console.log(player.gold);
        this.prizeText.setOrigin(0.5);
        this.canSpin = true;
        this.input.on("pointerdown",this.spinWheel,this);
        this.sound.add("wheelComplete");
    }



    spinWheel(){
        if(this.canSpin){
            this.prizeText.setText("");

            let rounds = Phaser.Math.Between(2,4);
            let degrees = Phaser.Math.Between(0,360);
            let prize = gameOptions.slices - 1 - Math.floor(degrees / (360 / gameOptions.slices));
            this.canSpin = false;

            this.tweens.add({
                targets: [this.wheel],
                angle: 360 * rounds + degrees,
                ease: "Cubic.easeOut",
                callbackScope:this,
                onComplete:function(tween){
                    this.prizeText.setText(gameOptions.slicePrizes[prize]);
                    console.log(gameOptions.slicePrizes[prize]);

                    this.canSpin = true;
                    this.sound.play("wheelComplete");
                    let parent = this;
                    setTimeout(function(){
                        changeScene(parent,gameOptions.slicePrizes[prize]);
                    },2000)
                }
            });
        }
    }




}

class fightGame extends Phaser.Scene{

    constructor(){
        super("fightGame");
        Phaser.Scene.call(this, {key:'fightGame'});
    }
    preload(){
        this.load.image("bg","assets/FightScreen.png");
        this.load.audio("damage","assets/SoundEffects/damageHit.mp3");
        this.load.audio("healing","assets/SoundEffects/healing.mp3");
        this.load.audio("defend","assets/SoundEffects/defend.mp3");
    }

    create(){

        this.add.image(500,500,"bg");
        let enemyDamage = Math.ceil(Math.random() * player.maxDamage);
        let enemyHealth = eneHealth = Math.ceil(Math.random()* player.maxHealth);
        let enemy = new character(enemyHealth,enemyDamage);
        console.log(enemy.health + "/" + enemy.damage);
        let downX, upX, downY, upY, threshold = 50;
        this.prizeMoney = Math.ceil((enemyDamage + enemyHealth) / 3);
        this.input.on('pointerdown', function (pointer) {
            downX = pointer.x;
            downY = pointer.y;
        });

        this.enemyHealthText = this.add.text(250,25,eneHealth,{
            font: "bold 32px Arial",
            align: "center",
            color:"white"
        });

        this.playerHealthText = this.add.text(150,860,player.health,{
            font: "bold 32px Arial",
            align: "center",
            color:"white"
        });

        this.playerPotionsText = this.add.text(150,905,player.potions,{
            font: "bold 32px Arial",
            align: "center",
            color:"white"
        });

        this.playerMoneyText = this.add.text(150,947,player.gold,{
            font: "bold 32px Arial",
            align: "center",
            color:"white"
        });

        this.sound.add("damage");
        this.sound.add("healing");
        this.sound.add("defend");
        let music = this;
        this.input.on('pointerup', function (pointer) {
            upX = pointer.x;
            upY = pointer.y;
            if (upX < downX - threshold){
                heal(player,enemy);
                music.sound.play("healing");
                console.log("swipeleft");
            } else if (upX > downX + threshold) {
                heal(player,enemy);
                music.sound.play("healing");
                console.log("swiperight");
            } else if (upY < downY - threshold) {
                fight(enemy,player);
                music.sound.play("damage");
                console.log("swipeup");
            } else if (upY > downY + threshold) {
                defend(enemy);
                music.sound.play("defend");
                console.log("swipedown");
            }
        });


    }

    update(){
        if(player.health < 0 || eneHealth < 0){

            if(eneHealth < 0){
                if(upgraded){
                    this.prizeMoney *= 3;
                    upgraded = false;
                }
                player.gold += this.prizeMoney;
            }else{
                player = new playerClass(5,1);
            }

            this.scene.start("playGame");
        }

        this.enemyHealthText.setText(eneHealth);
        this.playerHealthText.setText(player.health);
        this.playerPotionsText.setText(player.potions);
        this.playerMoneyText.setText(player.gold);
    }


}

class shopGame extends Phaser.Scene{


    constructor(){
        super("shopGame");
        Phaser.Scene.call(this, {key:'shopGame'});
    }
    preload(){
        console.log("IN Shop");
        this.load.spritesheet("buttons","assets/Buttons.png", {frameWidth:200,frameHeight:50});
        this.load.spritesheet("backbuttons","assets/BackButtons.png", {frameWidth:200, frameHeight:50});
        this.load.spritesheet("buyButtons","assets/BuyButtons.png",{frameWidth:50,frameHeight:50});
        this.load.audio("shopBuy","assets/SoundEffects/ShopBuy.mp3");
    }
    create(){

        //#region Text + Buttons
        this.healthText = this.add.text(200 ,85,"HEALTH UPGRADE",{
            font: "bold 32px Arial",
            align: "center",
            color:"BLACK"
        });

        this.damageText = this.add.text(200 ,185,"DAMAGE UPGRADE",{
            font: "bold 32px Arial",
            align: "center",
            color:"BLACK"
        });

        this.defenseText = this.add.text(200 ,285,"DEFENSE UPGRADE",{
            font: "bold 32px Arial",
            align: "center",
            color:"BLACK"
        });

        this.potionsText = this.add.text(200 ,550,"BUY POTIONS",{
            font: "bold 32px Arial",
            align: "center",
            color:"BLACK"
        });

        this.healthText = this.add.text(625 ,550,"BUY HEALTH",{
            font: "bold 32px Arial",
            align: "center",
            color:"BLACK"
        });

        this.potionOne = this.add.text(50 ,650,"ONE POTION",{
            font: "bold 32px Arial",
            align: "center",
            color:"BLACK"
        });

        this.potionMoney = this.add.text(50 ,750,"MONEY'S WORTH",{
            font: "bold 32px Arial",
            align: "center",
            color:"BLACK"
        });

        this.healFull = this.add.text(550 ,705,"FULL HEAL",{
            font: "bold 32px Arial",
            align: "center",
            color:"BLACK"
        });

        this.healthMoney = this.add.text(550 ,805,"MONEY'S WORTH",{
            font: "bold 32px Arial",
            align: "center",
            color:"BLACK"
        });

        this.currentHealth = this.add.text(850,85,player.health,{
            font: "bold 32px Arial",
            align: "center",
            color:"BLACK"
        });

        this.currentDamage = this.add.text(850,185,player.damage,{
            font: "bold 32px Arial",
            align: "center",
            color:"BLACK"
        });

        this.currentDefense = this.add.text(850,285,player.defense,{
            font: "bold 32px Arial",
            align: "center",
            color:"BLACK"
        });

        this.currentPAmount = this.add.text(50 ,850,"CURRENT POTIONS",{
            font: "bold 32px Arial",
            align: "center",
            color:"BLACK"
        });

        this.currentPotions = this.add.text(450,850,player.potions,{
            font: "bold 32px Arial",
            align: "center",
            color:"BLACK"
        });

        this.currentGold = this.add.text(350 ,10,player.gold,{
            font: "bold 32px Arial",
            align: "center",
            color: "YELLOW"
        });

        this.goldText = this.add.text(225 ,10,"GOLD",{
            font: "bold 32px Arial",
            align: "center",
            color: "YELLOW"
        });

        const healthButton = new BasicButton({
            'scene' : this,
            'key' : 'buttons',
            'up' : 0,
            'over' : 1,
            'down':2,
            'x': 700,
            'y': 100
        });

        const damageButton = new BasicButton({
            'scene' : this,
            'key' : 'buttons',
            'up' : 0,
            'over' : 1,
            'down':2,
            'x': 700,
            'y': 200
        });

        const defenseButton = new BasicButton({
            'scene' : this,
            'key' : 'buttons',
            'up' : 0,
            'over' : 1,
            'down':2,
            'x': 700,
            'y': 300
        });

        const backButton = new BasicButton({
            'scene' : this,
            'key' : 'backbuttons',
            'up' : 0,
            'over' : 1,
            'down':2,
            'x': 100,
            'y': 25
        });

        const potionMoneyButton = new BasicButton({
            'scene' : this,
            'key' : 'buyButtons',
            'up' : 0,
            'over' : 1,
            'down':2,
            'x': 463,
            'y': 765
        });

        const potionOneButton = new BasicButton({
            'scene' : this,
            'key' : 'buyButtons',
            'up' : 0,
            'over' : 1,
            'down':2,
            'x': 463,
            'y': 665
        });

        const healthMoneyButton = new BasicButton({
            'scene' : this,
            'key' : 'buyButtons',
            'up' : 0,
            'over' : 1,
            'down':2,
            'x': 913,
            'y': 820
        });

        const healthFullButton = new BasicButton({
            'scene' : this,
            'key' : 'buyButtons',
            'up' : 0,
            'over' : 1,
            'down':2,
            'x': 913,
            'y': 720
        });
        //#endregion

        //#region button Effects
        healthButton.on('pointerdown',this.onHealth,this);
        damageButton.on('pointerdown',this.onDamage,this);
        defenseButton.on('pointerdown',this.onDefense,this);
        backButton.on('pointerdown',this.onBack,this);
        potionOneButton.on('pointerdown',this.onPotion,this);
        potionMoneyButton.on('pointerdown', this.onPotionMoney,this);
        healthFullButton.on('pointerdown',this.onFullHeal,this);
        healthMoneyButton.on('pointerdown',this.onMoneyHeal,this);
        //#endregion

        this.lifeMoney = 20 + (player.health - player.baseHealth) * 5;
        this.attackMoney = 20 + (player.damage - player.baseAttack) * 5;
        this.defenseMoney = 20 + (player.defense - player.baseDefense) * 10;

        this.potionMoney = 15;
        this.healMoney = 10;

        if(upgraded){
            this.lifeMoney = Math.floor(this.lifeMoney / 2);
            this.attackMoney = Math.floor(this.attackMoney / 2);
            this.defenseMoney = Math.floor(this.defenseMoney / 2);
            this.potionMoney = Math.floor(this.potionMoney / 2);
            this.healMoney = Math.floor(this.healMoney / 2);
        }

        this.sound.add("shopBuy");
    }

    onHealth(){
        if(player.gold >= this.lifeMoney){
            player.gold -= this.lifeMoney;
            player.maxHealth += 2;
            player.health = player.maxHealth;
            this.currentHealth.setText(player.health);
            this.currentGold.setText(player.gold);
            this.sound.play("shopBuy");
        }

    }

    onDamage(){
        if(player.gold >= this.attackMoney){
            player.gold -= this.attackMoney;
            player.damage += 1;
            this.currentDamage.setText(player.damage);
            this.currentGold.setText(player.gold);
            this.sound.play("shopBuy");
        }
    }

    onDefense(){
        if(player.gold >= this.defenseMoney){
            player.gold -= this.defenseMoney;
            player.defense += .5;
            this.currentDefense.setText(player.defense);
            this.currentGold.setText(player.gold);
            this.sound.play("shopBuy");
        }
    }

    onBack(){
        upgraded = false;
        this.scene.start("playGame");
    }

    onPotion(){
        if(player.gold >= this.potionMoney){
            player.gold -= this.potionMoney;
            player.potions++;
            this.currentPotions.setText(player.potions);
            this.currentGold.setText(player.gold);
            this.sound.play("shopBuy");
        }
    }

    onPotionMoney(){
        let amount = Math.floor(player.gold/this.potionMoney);
        player.gold -= amount * this.potionMoney;
        player.potions += amount;
        this.currentPotions.setText(player.potions);
        this.currentGold.setText(player.gold);
        this.sound.play("shopBuy");
    }

    onFullHeal(){
        let toHeal = player.maxHealth - player.health;
        if(player.gold >= this.healMoney){
            player.gold -= toHeal * this.healMoney;
            player.health = player.maxHealth;
            this.currentGold.setText(player.gold);
            this.sound.play("shopBuy");
        }
    }

    onMoneyHeal(){
        let amount = Math.floor(player.gold / this.healMoney);
        if(amount > player.maxHealth - player.health) this.onFullHeal();
        else{
            player.gold -= amount * this.healFull;
            player.health += amount;
            this.currentGold.setText(player.gold);
            this.sound.play("shopBuy");
        }

    }

}

class BasicButton extends Phaser.GameObjects.Sprite{
    constructor(config){

        if(!config.scene){
            return;
        }

        if(!config.key){
            return;
        }

        if(!config.up){
            config.up = 0;
        }

        if (!config.down) {
            config.down = config.up;
        }

        if (!config.over) {
            config.over = config.up;
        }

        super(config.scene,0,0,config.key,config.up);
//if there is an x assign it
        if (config.x) {
            this.x = config.x;
        }
//if there is an x assign it
        if (config.y) {
            this.y = config.y;
        }


        config.scene.add.existing(this);
        this.config = config;
        this.setInteractive();
        this.on('pointerdown',this.onDown,this);
        this.on('pointerup',this.onUp,this);
        this.on('pointerover',this.onOver,this);
        this.on('pointerout',this.onUp,this);


    }

    onDown(){
        this.setFrame(this.config.down);
    }

    onOver(){
        this.setFrame(this.config.over);
    }

    onUp(){
        this.setFrame(this.config.up);
    }


}

class upgradeGame extends Phaser.Scene{

    constructor(){
        super("upgradeGame");
        Phaser.Scene.call(this, {key:'upgradeGame'});
    }
    preload(){
        this.load.spritesheet("fightButtons","assets/FightButtons.png", {frameWidth:200,frameHeight:50});
        this.load.spritesheet("gambleButtons","assets/GambleButtons.png", {frameWidth:200, frameHeight:50});
        this.load.spritesheet("shopButtons","assets/ShopButtons.png",{frameWidth:200,frameHeight:50});
    }
    create(){
        upgraded = true;

        const fightButton = new BasicButton({
            'scene' : this,
            'key' : 'fightButtons',
            'up' : 0,
            'over' : 1,
            'down':2,
            'x': 500,
            'y': 250
        });

        const gambleButton = new BasicButton({
            'scene' : this,
            'key' : 'gambleButtons',
            'up' : 0,
            'over' : 1,
            'down':2,
            'x': 250,
            'y': 625
        });

        const shopButton = new BasicButton({
            'scene' : this,
            'key' : 'shopButtons',
            'up' : 0,
            'over' : 1,
            'down':2,
            'x': 750,
            'y': 625
        });

        fightButton.on('pointerdown',this.onFight,this);
        gambleButton.on('pointerdown',this.onGamble,this);
        shopButton.on('pointerdown',this.onShop,this);
    }

    onFight(){
        this.scene.start("fightGame");
    }

    onGamble(){
        this.scene.start("gambleGame");
    }

    onShop(){
        this.scene.start("shopGame");
    }
}

class gambleGame extends Phaser.Scene{


    constructor(){
        super("gambleGame");
        Phaser.Scene.call(this, {key:'gambleGame'});

    }
    preload(){
        this.load.spritesheet("backbuttons","assets/BackButtons.png", {frameWidth:200, frameHeight:50});
        this.load.spritesheet("highRisk","assets/HighRisk.png", {frameWidth:200, frameHeight:50});
        this.load.spritesheet("lowRisk","assets/LowRisk.png", {frameWidth:200, frameHeight:50});
    }
    create(){
        const backButton = new BasicButton({
            'scene' : this,
            'key' : 'backbuttons',
            'up' : 0,
            'over' : 1,
            'down':2,
            'x': 100,
            'y': 25
        });

        const highButton = new BasicButton({
            'scene' : this,
            'key' : 'highRisk',
            'up' : 0,
            'over' : 1,
            'down':2,
            'x': 750,
            'y': 500
        });

        const lowButton = new BasicButton({
            'scene' : this,
            'key' : 'lowRisk',
            'up' : 0,
            'over' : 1,
            'down':2,
            'x': 250,
            'y': 500
        });

        backButton.on('pointerdown',this.onBack,this);
        highButton.on('pointerdown',this.onHigh,this);
        lowButton.on('pointerdown',this.onLow,this);

    }

    onBack(){
        this.scene.start("playGame");
    }
    onHigh(){
        this.scene.start('highGamble');
    }
    onLow(){
        this.scene.start('lowGamble');
    }
}

class highGamble extends Phaser.Scene{

    constructor(){
        super("highGamble");
        Phaser.Scene.call(this, {key:'highGamble'});
    }

    preload(){
        this.load.spritesheet("backbuttons","assets/BackButtons.png", {frameWidth:200, frameHeight:50});
        this.load.spritesheet("higherButton","assets/HigherButton.png", {frameWidth:200, frameHeight:50});
        this.load.spritesheet("lowerButton","assets/LowerButton.png", {frameWidth:200, frameHeight:50});
        this.load.spritesheet("low2Risk","assets/2xButtons.png", {frameWidth:50, frameHeight:50});
        this.load.spritesheet("low3Risk","assets/3xButtons.png", {frameWidth:50, frameHeight:50});
        this.load.spritesheet("high2Risk","assets/2xButtons.png", {frameWidth:50, frameHeight:50});
        this.load.spritesheet("high3Risk","assets/3xButtons.png", {frameWidth:50, frameHeight:50});
        this.load.spritesheet("startButton","assets/StartButton.png", {frameWidth:200, frameHeight:50});

        this.load.audio("success","assets/SoundEffects/success.mp3");
        this.load.audio("fail","assets/SoundEffects/fail.mp3");

    }

    create(){
        const backButton = new BasicButton({
            'scene' : this,
            'key' : 'backbuttons',
            'up' : 0,
            'over' : 1,
            'down':2,
            'x': 100,
            'y': 25
        });

        this.higherButton = new BasicButton({
            'scene' : this,
            'key' : 'higherButton',
            'up' : 0,
            'over' : 1,
            'down':2,
            'x': 750,
            'y': 500
        });

        this.lowerButton = new BasicButton({
            'scene' : this,
            'key' : 'lowerButton',
            'up' : 0,
            'over' : 1,
            'down':2,
            'x': 250,
            'y': 500
        });

        this.higher2Button = new BasicButton({
            'scene' : this,
            'key' : 'high2Risk',
            'up' : 0,
            'over' : 1,
            'down':2,
            'x': 675,
            'y': 575
        });

        this.higher3Button = new BasicButton({
            'scene' : this,
            'key' : 'high3Risk',
            'up' : 0,
            'over' : 1,
            'down':2,
            'x': 825,
            'y': 575
        });

        this.lower2Button = new BasicButton({
            'scene' : this,
            'key' : 'low2Risk',
            'up' : 0,
            'over' : 1,
            'down':2,
            'x': 175,
            'y': 575
        });

        this.lower3Button = new BasicButton({
            'scene' : this,
            'key' : 'low3Risk',
            'up' : 0,
            'over' : 1,
            'down':2,
            'x': 325,
            'y': 575
        });

        this.startButton = new BasicButton({
            'scene' : this,
            'key' : 'startButton',
            'up' : 0,
            'over' : 1,
            'down':2,
            'x': 500,
            'y': 250
        });

        this.result = this.add.text(500 ,900,"",{
            font: "bold 32px Arial",
            align: "center",
            color:"BLACK"
        });

        this.number = this.add.text(500 ,350,"",{
            font: "bold 32px Arial",
            align: "center",
            color:"BLACK"
        });

        this.sound.add("success");
        this.sound.add("fail");

        this.numberValue;
        this.bet;

        this.gambled = false;
        this.higherButton.setActive(false).setVisible(false);
        this.lowerButton.setActive(false).setVisible(false);
        this.higher2Button.setActive(false).setVisible(false);
        this.higher3Button.setActive(false).setVisible(false);
        this.lower2Button.setActive(false).setVisible(false);
        this.lower3Button.setActive(false).setVisible(false);

        backButton.on('pointerdown',this.onBack,this);
        this.higherButton.on('pointerdown',this.onHigh,this);
        this.lowerButton.on('pointerdown',this.onLow,this);
        this.higher2Button.on('pointerdown',this.onHigh2,this);
        this.higher3Button.on('pointerdown',this.onHigh3,this);
        this.lower2Button.on('pointerdown',this.onLow2,this);
        this.lower3Button.on('pointerdown',this.onLow3,this);
        this.startButton.on('pointerdown',this.onStart,this);

    }

    onBack(){
        if(!this.gambled){
            this.scene.start("gambleGame");
        }
        else{
            this.scene.start("playGame");
        }
    }

    onHigh(){
        this.lower2Button.setActive(false).setVisible(false);
        this.lower3Button.setActive(false).setVisible(false);
        this.higher2Button.setActive(true).setVisible(true);
        this.higher3Button.setActive(true).setVisible(true);
    }

    onLow(){
        this.lower2Button.setActive(true).setVisible(true);
        this.lower3Button.setActive(true).setVisible(true);
        this.higher2Button.setActive(false).setVisible(false);
        this.higher3Button.setActive(false).setVisible(false);
    }

    onHigh3(){
        let multiplier = 5;
        if(upgraded) multiplier = 7;
        console.log(multiplier);
        if(this.numberValue / 3 > this.number.text){
            this.result.setText("WIN!: " + this.numberValue);
            this.sound.play("success");
            player.gold += multiplier*this.bet;
        }else{
            this.sound.play("fail");
            this.result.setText("Lose: "+this.numberValue);
        }

        this.higherButton.setActive(false).setVisible(false);
        this.lowerButton.setActive(false).setVisible(false);
        this.higher2Button.setActive(false).setVisible(false);
        this.higher3Button.setActive(false).setVisible(false);
    }

    onHigh2(){
        let multiplier = 3;
        if(upgraded) multiplier = 5;
        console.log(multiplier);
        if(this.numberValue / 2 > this.number.text){
            this.result.setText("WIN!: " + this.numberValue);
            this.sound.play("success");
            player.gold += multiplier*this.bet;
        }else{
            this.sound.play("fail");
            this.result.setText("Lose: "+this.numberValue);
        }

        this.higherButton.setActive(false).setVisible(false);
        this.lowerButton.setActive(false).setVisible(false);
        this.higher2Button.setActive(false).setVisible(false);
        this.higher3Button.setActive(false).setVisible(false);
    }

    onLow3(){
        let multiplier = 5;
        if(upgraded) multiplier = 7;
        console.log(multiplier);
        if(this.numberValue * 3 < this.number.text){
            this.result.setText("WIN!: " + this.numberValue);
            this.sound.play("success");
            player.gold += multiplier*this.bet;
        }else{
            this.sound.play("fail");
            this.result.setText("Lose: "+this.numberValue);
        }

        this.higherButton.setActive(false).setVisible(false);
        this.lowerButton.setActive(false).setVisible(false);
        this.lower2Button.setActive(false).setVisible(false);
        this.lower3Button.setActive(false).setVisible(false);
    }

    onLow2(){
        let multiplier = 3;
        if(upgraded) multiplier = 5;

        console.log(multiplier);
        if(this.numberValue * 2 < this.number.text){
            this.result.setText("WIN!: " + this.numberValue);
            this.sound.play("success");
            player.gold += multiplier*this.bet;
        }else{
            this.sound.play("fail");
            this.result.setText("Lose: "+this.numberValue);
        }

        this.higherButton.setActive(false).setVisible(false);
        this.lowerButton.setActive(false).setVisible(false);
        this.lower2Button.setActive(false).setVisible(false);
        this.lower3Button.setActive(false).setVisible(false);
    }

    onStart(){

        let input = prompt("Please bet gold:", "0");
        let random;
        let result;
        if(input == null || input == ""){
            this.bet = -1;
            result = "No money bet, please give an amount greater than 0";
        }else if(input > player.gold){
            this.bet = -1;
            result = "You do not have enough money";
        }
        else{
            this.bet = input;
        }

        if(this.bet > 0){
            this.higherButton.setActive(true).setVisible(true);
            this.lowerButton.setActive(true).setVisible(true);
            random = Math.floor(Math.random() * (12-1))+1;
            this.number.setText(random);
            this.result.setText("");
            this.startButton.setActive(false).setVisible(false);
            this.numberValue = Math.floor(Math.random() * (12-1))+1;
            player.gold -= this.bet;
            this.gambled = true;
            console.log(this.numberValue);
        }else{
            this.result.setText(result);
        }
    }
}

class lowGamble extends Phaser.Scene{

    constructor(){
        super("lowGamble");
        Phaser.Scene.call(this, {key:'lowGamble'});
    }

    preload(){
        this.load.spritesheet("backbuttons","assets/BackButtons.png", {frameWidth:200, frameHeight:50});
        this.load.spritesheet("higherButton","assets/HigherButton.png", {frameWidth:200, frameHeight:50});
        this.load.spritesheet("lowerButton","assets/LowerButton.png", {frameWidth:200, frameHeight:50});
        this.load.spritesheet("startButton","assets/StartButton.png", {frameWidth:200, frameHeight:50});
        this.load.audio("success","assets/SoundEffects/success.mp3");
        this.load.audio("fail","assets/SoundEffects/fail.mp3");
        console.log("LOWGAMBLE");
    }

    create(){
        const backButton = new BasicButton({
            'scene' : this,
            'key' : 'backbuttons',
            'up' : 0,
            'over' : 1,
            'down':2,
            'x': 100,
            'y': 25
        });

        this.higherButton = new BasicButton({
            'scene' : this,
            'key' : 'higherButton',
            'up' : 0,
            'over' : 1,
            'down':2,
            'x': 750,
            'y': 500
        });

        this.lowerButton = new BasicButton({
            'scene' : this,
            'key' : 'lowerButton',
            'up' : 0,
            'over' : 1,
            'down':2,
            'x': 250,
            'y': 500
        });

        this.startButton = new BasicButton({
            'scene' : this,
            'key' : 'startButton',
            'up' : 0,
            'over' : 1,
            'down':2,
            'x': 500,
            'y': 250
        });

        this.result = this.add.text(500 ,900,"",{
            font: "bold 32px Arial",
            align: "center",
            color:"BLACK"
        });

        this.number = this.add.text(500 ,350,"",{
            font: "bold 32px Arial",
            align: "center",
            color:"BLACK"
        });

        this.numberValue;
        this.bet;
        this.sound.add("success");
        this.sound.add("fail");
        this.multiply = 2;
        if(upgraded) this.multiply = 3;
        this.gambled = false;
        this.higherButton.setActive(false).setVisible(false);
        this.lowerButton.setActive(false).setVisible(false);

        backButton.on('pointerdown',this.onBack,this);
        this.higherButton.on('pointerdown',this.onHigh,this);
        this.lowerButton.on('pointerdown',this.onLow,this);
        this.startButton.on('pointerdown',this.onStart,this);

    }

    onBack(){
        if(!this.gambled){
            this.scene.start("gambleGame");
        }
        else{
            this.scene.start("playGame");
        }
    }

    onHigh(){
        if(this.numberValue > this.number.text){
            this.result.setText("WIN!: " + this.numberValue);
            this.sound.play("success");
            player.gold += this.multiply*this.bet;
        }else{
            this.result.setText("Lose: "+this.numberValue);
            this.sound.play("fail");
        }

        this.higherButton.setActive(false).setVisible(false);
        this.lowerButton.setActive(false).setVisible(false);
    }

    onLow(){
        if(this.numberValue < this.number.text){
            this.result.setText("WIN!: " + this.numberValue);
            this.sound.play("success");
            player.gold += this.multiply*this.bet;
        }else{
            this.sound.play("fail");
            this.result.setText("Lose: "+this.numberValue);
        }

        this.higherButton.setActive(false).setVisible(false);
        this.lowerButton.setActive(false).setVisible(false);
    }

    onStart(){

        let input = prompt("Please bet gold:", "0");
        let random;
        let result;
        if(input == null || input == ""){
            this.bet = -1;
            result = "No money bet, please give an amount greater than 0";
        }else if(input > player.gold){
            this.bet = -1;
            result = "You do not have enough money";
        }
        else{
            this.bet = input;
        }

        if(this.bet > 0){
            this.higherButton.setActive(true).setVisible(true);
            this.lowerButton.setActive(true).setVisible(true);
            random = Math.floor(Math.random() * (12-1))+1;
            this.number.setText(random);
            this.result.setText("");
            this.startButton.setActive(false).setVisible(false);
            this.numberValue = Math.floor(Math.random() * (12-1))+1;
            player.gold -= this.bet;
            this.gambled = true;
        }else{
            this.result.setText(result);
        }
    }
}

function resize(){
    let canvas = document.querySelector("canvas");
    let windowWidth = window.innerWidth;
    let windowHeight = window.innerHeight;
    let windowRatio = windowWidth / windowHeight;
    let gameRatio = game.config.width / game.config.height;

    if(windowRatio < gameRatio){
        canvas.style.width = windowWidth + "px";
        canvas.style.height = (windowWidth * gameRatio) + "px";
    }else{
        canvas.style.width = (windowHeight * gameRatio) + "px";
        canvas.style.height = windowHeight + "px";
    }
}

function fight(enemy,player){
    enemy.receiveDamage(player.damage);
    eneHealth -= player.damage;
    player.receiveDamage(enemy.damage);
}

function defend(enemy){
    player.receiveDamage(Math.ceil(enemy.damage/player.defense));
}

function heal(player,enemy){
    if(player.potions > 0 && player.health < player.maxHealth){
        player.receiveDamage(-10);
        player.receiveDamage(enemy.damage);
        player.potions--;
    }
}

function changeScene(parent,scene){
    parent.scene.start(scene);
}